
### 深入学习 redux-saga

这个模块要从我使用[dva](https://github.com/dvajs/dva)开始说起，不得不说[dva](https://github.com/dvajs/dva)是个很不错的框架，它是蚂蚁金服在react上的技术的总结沉淀，以及配套使用的[ant-design](https://mobile.ant.design/index-cn),都是非常不错的框架，之后可能会继续介绍这些优秀的库，今天的主角是[redux-saga](https://redux-saga.js.org/)，在dva中，redux-saga是一个redux的中间件
